package com.alfa.application.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alfa.application.model.dog.Dog;
import com.alfa.application.model.dog.ResDogDetail;
import com.alfa.dao.impl.DogServiceImpl;
import com.alfa.dao.service.DogService;

import io.swagger.annotations.ApiOperation;

@RestController
public class DogRestController {

	DogService dogService;
	
	@Autowired
	public DogRestController(DogService dogService) {
		super();
		this.dogService = dogService;
	}

	@RequestMapping(path = "/test", method = RequestMethod.GET)
	public String tester() {
		String test = "Error!";
		
		try {
			test = dogService.tester();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return test;
	}
	
	@ApiOperation("Mengembalikan list seluruh dogs")
	@RequestMapping(method = RequestMethod.GET, path = "/dogs", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Dog> getDogsAll() {
		List<Dog> response = dogService.getDogAll();

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/dogs/{breed}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResDogDetail getUser(@RequestParam String breed) {
		ResDogDetail response = dogService.getDogDetail(breed);
		return response;
	}
}
