package com.alfa.application.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alfa.application.model.dog.Dog;
import com.alfa.application.model.dog.ResDogDetail;
import com.alfa.application.model.employee.Employee;
import com.alfa.application.model.employee.ResEmpAll;
import com.alfa.dao.mapper.EmployeeMapper;
import com.alfa.dao.service.DogService;
import com.alfa.dao.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

@RestController
public class EmployeeRestController {
	EmployeeService empService;
	
	@Autowired
	public EmployeeRestController(EmployeeService empService) {
		super();
		this.empService = empService;
	}
	
	@ApiOperation("Mengembalikan list seluruh employee")
	@RequestMapping(method = RequestMethod.GET, path = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Employee> getDogsAll() {
		List<Employee> response = empService.getEmpAll();
		return response;
	}

	@ApiOperation("Mengembalikan employee individual")
	@RequestMapping(method = RequestMethod.GET, path = "/employee/{empId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Employee getUser(@RequestParam Integer empId) {
		Employee response = empService.getEmpDetail(empId);
		return response;
	}
	
	@ApiOperation("Menambahkan Employee Baru")
	@RequestMapping(method = RequestMethod.PUT, path = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResEmpAll insertEmployee(@RequestBody Employee emp) {
		ResEmpAll response = empService.insertEmployee(emp);
		return response;
	}
	
	@ApiOperation("Menghapus employee")
	@RequestMapping(method = RequestMethod.DELETE, path = "/employee/{empId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResEmpAll deleteEmployee(@RequestParam Integer empId) {
		ResEmpAll response = empService.deleteEmployee(empId);
		return response;
	}
	
	@ApiOperation("Mengupdate employee")
	@RequestMapping(method = RequestMethod.POST, path = "/employee/{empId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResEmpAll updateEmployee(@RequestParam Integer empId, @RequestBody Employee emp) {
		ResEmpAll response = empService.updateEmployee(empId, emp);
		return response;
	}
}
