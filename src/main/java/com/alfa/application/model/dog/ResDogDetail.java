package com.alfa.application.model.dog;

import java.util.List;

import com.alfa.application.model.common.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Response untuk Get Dog Detail")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResDogDetail extends BaseResponse{

	@ApiModelProperty(notes = "Nama", required = true, position = 0)
	private Dog dog;

	public ResDogDetail() {
		super();
	}

	public Dog getDog() {
		return dog;
	}

	public void setDog(Dog dog) {
		this.dog = dog;
	}

	
}
