package com.alfa.application.model.dog;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Data Dog All dari ${dog.api.all.url}")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiDogAllResp {
	
	private HashMap<String, Object> message;

	private String status;
	
	public ApiDogAllResp() {
		super();
	}

	public HashMap<String, Object> getMessage() {
		return message;
	}

	public void setMessage(HashMap<String, Object> message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ApiDogAllResp [message=" + message + "]";
	}

}
