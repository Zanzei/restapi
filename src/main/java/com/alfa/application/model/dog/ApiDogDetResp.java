package com.alfa.application.model.dog;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Data Dog Detail")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiDogDetResp {
	
	private List<String> message;

	private String status;
	
	public ApiDogDetResp() {
		super();
	}

	public List<String> getMessage() {
		return message;
	}

	public void setMessage(List<String> message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ApiDogDetResp [message=" + message + ", status=" + status + "]";
	}
	
	
}
