package com.alfa.application.model.dog;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.alfa.application.model.common.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Data Dog")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Dog extends BaseResponse{

	@NotNull
	@ApiModelProperty(notes = "Nama Breed", example = "beagle", required = true, position = 0)
	private String breed;

	@JsonInclude(JsonInclude.Include.ALWAYS)
	@JsonProperty("sub_breed")
	@ApiModelProperty(notes = "Sub Breed", required = false, position = 1)
	private List<Dog> subBreed;

	@NotNull
	@ApiModelProperty(notes = "List Images", required = false, position = 2)
	private List<String> images;
	
	public Dog() {
		super();
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public List<Dog> getSubBreed() {
		return subBreed;
	}

	public void setSubBreed(List<Dog> subBreed) {
		this.subBreed = subBreed;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

}
