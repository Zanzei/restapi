package com.alfa.application.model.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {

	@ApiModelProperty(notes = "Response Code", example = "00", required = true, readOnly = true, position = 99)
	private String responseCode;
	
	@ApiModelProperty(notes = "Response Message", example = "SUCCESS", required = true, readOnly = true, position = 99)
	private String responseMessage;
	

	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
	
}
