package com.alfa.application.model.employee;

import com.alfa.application.model.common.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Response untuk Get Dog Detail")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResEmpAll extends BaseResponse{
	
	@ApiModelProperty(notes = "Message tambahan", required = true, position = 0)
	private String message;

	public ResEmpAll() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
