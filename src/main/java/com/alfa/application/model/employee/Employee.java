package com.alfa.application.model.employee;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.alfa.application.model.common.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
@ApiModel(description = "Data Employee")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Employee extends BaseResponse{
	
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@NotNull
	@ApiModelProperty(notes = "Employee ID", example = "1", required = true, readOnly = true, position = 0)
	private Integer id;
	
	@JsonProperty("full_name")
	@NotNull
	@ApiModelProperty(notes = "Employee Full Name", example = "Adi Purnama", required = true, position = 1)
	private String fullName;
	
	@NotNull
	@ApiModelProperty(notes = "Employee Address", example = "Depok", required = true, position = 2)
	private String address;
	
	@NotNull
	@ApiModelProperty(notes = "Employee DOB", example = "1985-02-20T16:00:22Z", required = true, position = 3)
	private Date dob;
	
	@JsonProperty("role_id")
	@NotNull
	@ApiModelProperty(notes = "Employee Role ID", example = "1", required = true, position = 4)
	private Integer roleId;
	
	@JsonProperty("role_name")
	@NotNull
	@ApiModelProperty(notes = "Employee Role NName", example = "Adi Purnama", required = true, 
			 		  readOnly = true, position = 5)
	private String roleName;
	
	@NotNull
	@ApiModelProperty(notes = "Employee Salary", example = "10000000", required = true, position = 6)
	private Integer salary;
	
	public Employee() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", fullName=" + fullName + ", address=" + address + ", dob=" + dob + ", roleId="
				+ roleId + ", roleName=" + roleName + ", salary=" + salary + "]";
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	
	
}
