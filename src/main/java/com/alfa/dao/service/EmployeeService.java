package com.alfa.dao.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alfa.application.model.employee.Employee;
import com.alfa.application.model.employee.ResEmpAll;

@Service
public interface EmployeeService {
	
	public List<Employee> getEmpAll();
	
	public Employee getEmpDetail(Integer empId);
	
	public ResEmpAll insertEmployee(Employee emp);
	
	public ResEmpAll deleteEmployee(Integer empId);
	
	public ResEmpAll updateEmployee(Integer empId, Employee emp);
}
