package com.alfa.dao.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alfa.application.model.dog.Dog;
import com.alfa.application.model.dog.ResDogDetail;

@Service
public interface DogService {

	public String tester();
	
	public List<Dog> getDogAll();

	public ResDogDetail getDogDetail(String breed);
}
