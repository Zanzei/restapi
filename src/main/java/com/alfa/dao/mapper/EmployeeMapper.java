package com.alfa.dao.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.alfa.application.model.employee.Employee;

@Mapper
public interface EmployeeMapper {
	
	@Select("SELECT * "
			+ "FROM employee e "
			+ "LEFT JOIN role r on r.role_id = e.role_id "
			+ "LEFT JOIN salary s on s.employee_id = e.id "
			+ "ORDER BY e.id ")
	public List<Employee> getEmpAll();
	
	@Select("SELECT role_name "
			+ "FROM role "
			+ "WHERE role_id = #{roleId} ")
	public String getRoleName(Integer roleId);
	
	@Select("SELECT * "
			+ "FROM employee e "
			+ "LEFT JOIN role r on r.role_id = e.role_id "
			+ "LEFT JOIN salary s on s.employee_id = e.id "
			+ "WHERE e.id = #{id} ")
	public Employee getEmpDetail(Integer id);
	
	@Delete("DELETE FROM employee "
			+ "WHERE id = #{id} ")
	public Integer deleteEmp(Integer id);
	
	@Delete("DELETE FROM salary "
			+ "WHERE employee_id = #{id} ")
	public Integer deleteSalary(Integer id);
	
	@Delete("UPDATE employee "
			+ "SET full_name = IFNULL(#{fullName}, full_name), "
			+ "    address = IFNULL(#{address}, address), "
			+ "	   dob = IFNULL(#{dob}, dob), "
			+ "    role_id = IFNULL(#{roleId}, role_id) "
			+ "WHERE id = #{id} ")
	public Integer updateEmp(Employee emp);
	
	@Delete("UPDATE salary "
			+ "SET salary = IFNULL(#{salary}, salary) "
			+ "WHERE employee_id = #{id} ")
	public Integer updateSalary(Employee emp);
	
	@Insert("INSERT INTO employee "
			+ "SET full_name = #{fullName}, "
			+ "    address = #{address}, "
			+ "    dob = #{dob}, "
			+ "	   role_id = #{roleId} ")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn="id")
	public Integer insertEmp(Employee emp);
	
	@Insert("INSERT INTO salary "
			+ "SET id = #{id}, "
			+ "    salary = #{salary} ")
	public Integer insertSalary(Employee emp);
	
}
