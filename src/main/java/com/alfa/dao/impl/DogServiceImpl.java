package com.alfa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.alfa.application.model.dog.ApiDogAllResp;
import com.alfa.application.model.dog.ApiDogDetResp;
import com.alfa.application.model.dog.Dog;
import com.alfa.application.model.dog.ResDogDetail;
import com.alfa.dao.mapper.DogMapper;
import com.alfa.dao.service.DogService;

@Component
//@Transactional("DogService")
public class DogServiceImpl implements DogService{

	DogMapper dogMapper;
	
	Environment env;

	RestTemplate template;
	
	@Autowired
	public DogServiceImpl(DogMapper dogMapper, Environment env) {
		super();
		this.dogMapper = dogMapper;
		this.env = env;
		template = new RestTemplate();
	}

	/*
	 * Mapping : /test
	 * @date    : 13 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	public String tester() {
		return "--- Working as Intended! ---";
	}

	/*
	 * Mapping : /dogs
	 * @date    : 13 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	public List<Dog> getDogAll() {
		List<Dog> listDogs = new ArrayList<>();
		
		try {
			//Vars
			String url = env.getProperty("dog.api.all.url");
			
			//Hit ke API sesuai URL
			ApiDogAllResp resp = template.getForObject(url, ApiDogAllResp.class);
			if(resp.getStatus() == null ) {
				throw new Exception(" Error hit ke " + url + ".");
			}
			
			//Iterate over HashMap from response
			HashMap<String, Object> mapBreed = resp.getMessage();
			for (Map.Entry<String, Object> e : mapBreed.entrySet()) {
				Dog breed = new Dog();
				breed.setBreed(e.getKey());
				
				//Iterate over sub-breeds
				if(e.getValue() != null ) {
					List<Dog> listSubBreed = new ArrayList<>();
					for(String s : (List<String>)e.getValue()) {
						Dog subBreed = new Dog();
						subBreed.setBreed(s);
						subBreed.setSubBreed(new ArrayList<>());
						listSubBreed.add(subBreed);
					}
					breed.setSubBreed(listSubBreed);
				}
				
				listDogs.add(breed);		
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			Dog err = new Dog();
			err.setResponseCode("01");
			err.setResponseMessage("INVALID DATA" + e.getMessage());
			listDogs.add(err);
		}
		
		return listDogs;
	}
	
	/*
	 * Mapping : /dogs/{breed}
	 * @date    : 13 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	public ResDogDetail getDogDetail(String breed) {
		ResDogDetail response = new ResDogDetail();
		
		try {
			//Vars
			Dog dog = new Dog();
			String urlSubBreed = env.getProperty("dog.api.byBreed.url");
			breed = breed.toLowerCase();
			
			//Hit ke API Sub Breed sesuai URL
			ApiDogDetResp respSubBreed = template.getForObject(urlSubBreed + breed + "/list", ApiDogDetResp.class);
			if(respSubBreed.getStatus() == null ) {
				throw new Exception(" Error hit ke " + urlSubBreed + ".");
			}
			
			//Hit ke API Images sesuai URL
			ApiDogDetResp respBreedImage = template.getForObject(urlSubBreed + breed + "/images", ApiDogDetResp.class);
			if(respBreedImage.getStatus() == null ) {
				throw new Exception(" Error hit ke " + urlSubBreed + ".");
			}
	
			//Iterate over sub breeds
			List<Dog> listSubBreed = new ArrayList<>();
			for(String s : respSubBreed.getMessage()) {
				Dog subBreed = new Dog();
				subBreed.setBreed(s);
				subBreed.setSubBreed(null);
				listSubBreed.add(subBreed);
			}
			dog.setSubBreed(listSubBreed);
			
			//Set
			dog.setBreed(breed);
			dog.setImages(respBreedImage.getMessage());
			
			//Response
			response.setDog(dog);
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
			
		} catch(Exception e) {
			e.printStackTrace();
			response.setResponseCode("01");
			response.setResponseMessage("INVALID DATA. " + e.getMessage());
		}
		
		return response;
	}
	
}
