package com.alfa.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alfa.application.model.employee.Employee;
import com.alfa.application.model.employee.ResEmpAll;
import com.alfa.dao.mapper.EmployeeMapper;
import com.alfa.dao.service.EmployeeService;

@Component
public class EmployeeServiceImpl implements EmployeeService{

	EmployeeMapper empMapper;
	
	Environment env;

	@Autowired
	public EmployeeServiceImpl(EmployeeMapper empMapper, Environment env) {
		super();
		this.empMapper = empMapper;
		this.env = env;
	}

	/*
	 * Mapping : /employee
	 * @date    : 14 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	public List<Employee> getEmpAll() {
		List<Employee> listEmps = new ArrayList<>();
		
		try {
			//Vars
			listEmps = empMapper.getEmpAll();
			
		} catch(Exception e) {
			e.printStackTrace();
			Employee err = new Employee();
			err.setResponseCode("01");
			err.setResponseMessage("INVALID DATA" + e.getMessage());
			listEmps.add(err);
		}
		
		return listEmps;
	}

	/*
	 * Mapping : /employee/{empId}
	 * @date    : 14 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	public Employee getEmpDetail(Integer empId) {
		Employee emp = new Employee(); 
		
		try {
			//Vars
			emp = empMapper.getEmpDetail(empId);
			
			//Error Checking if not found
			if(emp == null) {
				throw new Exception(". Employee not found!");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			emp = new Employee();
			emp.setResponseCode("01");
			emp.setResponseMessage("INVALID DATA" + e.getMessage());
		}
		
		return emp;
	}

	/*
	 * Mapping : /employee
	 * @date    : 14 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResEmpAll insertEmployee(Employee emp) {
		ResEmpAll response = new ResEmpAll(); 
		
		try {
			//Check : ROLE ada tidak
			String roleName = empMapper.getRoleName(emp.getRoleId());
			checkStatus(roleName, ". Role ID is invalid");
			
			//Insert : EMPLOYEE
			Integer status = empMapper.insertEmp(emp);
			checkStatus(status, ". Failed to insert employee.");
			
			//Insert : SALARY
			status = empMapper.insertSalary(emp);
			checkStatus(status, ". Failed to insert salary.");

			//Response
			response.setResponseCode("00");
			response.setResponseMessage("Berhasil insert employee");
			
		} catch(Exception e) {
			e.printStackTrace();
			response.setResponseCode("01");
			response.setResponseMessage("INVALID DATA" + e.getMessage());
		}
		
		return response;
	}

	/*
	 * Mapping : /employee/{empId}
	 * @date    : 14 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResEmpAll deleteEmployee(Integer empId) {
		ResEmpAll response = new ResEmpAll(); 
		
		try {
			//Delete : EMPLOYEE
			Integer status = empMapper.deleteEmp(empId);
			checkStatus(status, ". Failed to delete employee.");
			
			//Delete : SALARY
			status = empMapper.deleteSalary(empId);
			checkStatus(status, ". Failed to delete salary.");
			
			//Response
			response.setResponseCode("00");
			response.setResponseMessage("Berhasil delete employee");
			
		} catch(Exception e) {
			e.printStackTrace();
			response.setResponseCode("01");
			response.setResponseMessage("INVALID DATA" + e.getMessage());
		}
		
		return response;
	}

	/*
	 * Mapping : /employee/{empId}
	 * @date    : 14 Sept 2019
	 * @author  : Andre Christian
	 *  
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResEmpAll updateEmployee(Integer empId, Employee emp) {
		ResEmpAll response = new ResEmpAll(); 
		
		try {
			//Check : ROLE ada tidak
			if(emp.getRoleId() != null) {
				String roleName = empMapper.getRoleName(emp.getRoleId());
				checkStatus(roleName, ". Role ID is invalid!");	
			}

			//Check : EMPLOYEE ada tidak.
			Employee empCheck = empMapper.getEmpDetail(empId);
			checkStatus(empCheck, ". Employee ID is invalid!");
			
			//Update : Employee
			emp.setId(empId);
			Integer status = empMapper.updateEmp(emp);
			
			//Update : Salary
			status = empMapper.updateSalary(emp);
			
			//Response
			response.setResponseCode("00");
			response.setResponseMessage("Berhasil update employee");
			
		} catch(Exception e) {
			e.printStackTrace();
			response.setResponseCode("01");
			response.setResponseMessage("INVALID DATA" + e.getMessage());
		}
		
		return response;
	}
	
	//Error-Checking
	private void checkStatus(Object status, String wording) throws Exception {
		if(status == null) {
			throw new Exception(wording);
		}
	}
	
}
